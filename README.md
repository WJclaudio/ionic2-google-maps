# README #

Ionic 2 Google Maps project. 

Make sure to cordova plugin add cordova-plugin-geolocation
cordova plugin add cordova-plugin-googlemaps --variable API_KEY_FOR_ANDROID="YOUR_ANDROID_API_KEY_IS_HERE" --variable API_KEY_FOR_IOS="YOUR_IOS_API_KEY_IS_HERE"


Current issue: When changing state, the previous state overlaps the current one. I.E. if you drag the map after changing to Native you'll notice that the native map is underneath the non-native. 