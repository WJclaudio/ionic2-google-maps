import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation, GoogleMap, GoogleMapsEvent } from 'ionic-native';

declare var google: any

@Component({
  templateUrl: 'build/pages/mapNative/mapNative.html'
})

export class MapNativePage {
  map: any; // defined as a class member.
  constructor(public navCtrl: NavController) {
    this.map = null;

    this.loadMap();
  }

  loadMap() {
    this.map = new GoogleMap('map');

    // listen to MAP_READY event
    this.map.one(GoogleMapsEvent.MAP_READY).subscribe(() => console.log('Map is ready!'));

  }
}
